package com.greggor.SmartLock2;

interface SL2SInterface {
	void handleEvent(in boolean near);
	void handlePrefsChanged(in String key);
}
