/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2;

import com.greggor.SmartLock2.SmartLock2Service;
import com.greggor.SmartLock2.preference.SmartLock2SettingsFragment;
import com.greggor.SmartLock2.receivers.SmartLock2ProximityReceiver;
import com.greggor.log.Log;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DeviceAdminInfo;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
//import android.view.KeyEvent;


/**
 * Displays preferences screen
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2Activity extends Activity implements OnSharedPreferenceChangeListener {
	/**
	 * Service id
	 */
	private final int ACTIVITY_ID = 1235;
	/**
	 * Service connection
	 */
    private ServiceConnection mConnection = new ServiceConnection()
    {
            public void onServiceConnected(ComponentName className, IBinder service) {
            	mServiceInterface = SL2SInterface.Stub.asInterface((IBinder)service);
            	Log.d("Service connected");
            }
     
            public void onServiceDisconnected(ComponentName className) {
            	mServiceInterface = null;
            	Log.d("Service disconnected");
            }

    };

 	/**
	 * Device administrator request arbitrary ID
	 */
	public static final int ADD_DEVICE_ADMIN_REQUEST = 1000;

	/**
	 * Interface to service
	 */
	private SL2SInterface mServiceInterface;

	/**
	 * Suppress closing on BACK button pressed
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		//start listening to proximity sensor events if app is enabled in preferences
		if (prefs.getBoolean("enabled",false)) 
		{
			SmartLock2ProximityReceiver.start(this);
			SmartLock2Service.start(this, mConnection);
		} else if (prefs.getBoolean("show_notification", false)) {
			SmartLock2IdleService.start(this);
		}
		prefs.registerOnSharedPreferenceChangeListener(this);
		// load preference using Fragments manner
		getFragmentManager().beginTransaction().replace(android.R.id.content, new SmartLock2SettingsFragment()).commit();
	}

	@Override
	protected void onDestroy() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		// disconnect from service & leave it in background
		if (prefs.getBoolean("enabled",false)) 
			SmartLock2Service.disconnect(this, mConnection);
		prefs.unregisterOnSharedPreferenceChangeListener(this);
		super.onDestroy();
	}

	/*
	 * Handle preferences changes
	 * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
	 */
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		// handle enable / disable toggle
		if (key.equals("enabled")) { 
			if (prefs.getBoolean("enabled",false)) 
			{
				SmartLock2ProximityReceiver.start(this);
				SmartLock2Service.start(this, mConnection);
				if (prefs.getBoolean("show_notification", false)) {
					// turn off "Stopped" notification
					SmartLock2IdleService.stop(this);
				}
			}
			else
			{
				SmartLock2Service.stop(this, mConnection);
				SmartLock2ProximityReceiver.stop(this);
				if (prefs.getBoolean("show_notification", false)) {
					// turn on "Stopped" notification
					SmartLock2IdleService.start(this);
				}
			}
		} else if (key.equals("show_notification")) {
			if (prefs.getBoolean("enabled",false)) 
			{
				// Update service notification icon 
				SmartLock2ProximityReceiver.start(this);
				SmartLock2Service.start(this, mConnection);
			} 
			if (prefs.getBoolean("show_notification", false) && !prefs.getBoolean("enabled",false)) {
				// turn on "Stopped" notification only if "Running" notification is not shown
				SmartLock2IdleService.start(this);
			} else {
				SmartLock2IdleService.stop(this);
			}
		}
		else
			try {
				mServiceInterface.handlePrefsChanged(key);
			} catch (RemoteException e) {
				Log.e("Cannot update preferences! Service communication failure? %s", e);
			}
	}

	/**
	 * Workaround for Bluetooh voice control
	 */
	protected void onStart() {
		DevicePolicyManager dpm = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
		ComponentName adminName = new ComponentName(this, SmartLock2Service.MyAdmin.class);

		if (!dpm.isAdminActive(adminName)) {
			Log.d("Trying to get device admin rights");
			// try to become active - must happen here in this activity, to get result
			Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
			intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminName);
			intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Device locking and unlocking");
			intent.putExtra("force-locked", DeviceAdminInfo.USES_POLICY_FORCE_LOCK);
			startActivityForResult(intent, ADD_DEVICE_ADMIN_REQUEST);
		}
		super.onStart();
	}
	
	/**
	 * Workaround for Bluetooh voice control
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{	
		if (requestCode == ADD_DEVICE_ADMIN_REQUEST) 
		{
			if (resultCode == Activity.RESULT_OK) {
				// Has become the device administrator.
				Log.d("Got device admin rights");
			} else {
				//Canceled or failed.
				Log.d("Didn't get device admin rights!");
			}
		}
	}
}