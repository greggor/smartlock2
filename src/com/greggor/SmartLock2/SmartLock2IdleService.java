/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2;

import com.greggor.log.Log;
import com.greggor.SmartLock2.R;
import com.greggor.SmartLock2.SL2SInterface;
import com.greggor.SmartLock2.SmartLock2Activity;
import com.greggor.SmartLock2.SmartLock2Service;
import com.greggor.SmartLock2.SmartLock2Trace;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Triggers lock request basing on proximity sensor events.
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2IdleService extends Service {

	/**
	 * Service id
	 */
	private final int IDLE_SERVICE_ID = 1235;

	/**
	 * Start the service
	 */
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Read all required data from preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		Notification.Builder builder = new Notification.Builder(this);
			builder
			.setContentTitle("SmartLock2")
			.setContentText("Stopped")
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, SmartLock2Activity.class), 0));
		startForeground(IDLE_SERVICE_ID, builder.getNotification());
		Log.d("Starting");
		return START_STICKY;
	}

	public void onLowMemory() {
	}

	public void onDestroy() {
	}

	/**
	 * Helper function to start the service
	 * @param context current application context
	 * @param connection connection interface to newly created service
	 */
	public static void start(Context context)
	{
		context.startService(new Intent(context, SmartLock2IdleService.class));
	}

	/**
	 * Helper function to stop the service
	 * @param context current application context
	 * @param connection connection interface to the service
	 */
	public static void stop(Context context)
	{
		context.stopService(new Intent(context, SmartLock2IdleService.class));
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new IllegalArgumentException("Service cannot be bounded");
	}
}
