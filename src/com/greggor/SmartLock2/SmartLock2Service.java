/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2;

import java.io.DataOutputStream;
import java.io.IOException;

import com.greggor.log.Log;
import com.greggor.SmartLock2.R;
import com.greggor.SmartLock2.SL2SInterface;
import com.greggor.SmartLock2.receivers.SmartLock2PhoneStateReceiver;
import com.greggor.SmartLock2.receivers.SmartLock2ScreenReceiver;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.Service;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.PreferenceManager;

/**
 * Handles screen locking and unlocking
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2Service extends Service {

	/**
	 * Binder interface implementation
	 */
	private final SL2SInterface.Stub mBinder = new SL2SInterface.Stub() {
		public void handleEvent(boolean near) throws RemoteException {
			onSensorEvent(near);
		}

		@Override
		public void handlePrefsChanged(String key) throws RemoteException {
			onPrefsChanged(key);
		}
	};
	/**
	 * Restart service automatically when killed
	 */
	public static boolean Restart = false;
	/**
	 * Needed for getting device administrator rights
	 * @author Grzegorz Ożański (greggor@gmail.com)
	 *
	 */
	public static class MyAdmin extends DeviceAdminReceiver {
		// implement onEnabled(), onDisabled(),
	}
	/**
	 * Vibration schema - no vibration
	 */
	public static final int NO_VIBRATE = 0;
	/**
	 * Vibration schema - vibrate when locked and unlocked
	 */
	public static final int VIBRATE_ON_LOCK_UNLOCK = 1;
	/**
	 * Vibration schema - vibrate just about locking
	 */
	public static final int VIBRATE_ABOUT_LOCK = 2;

	/**
	 * Auto-relock schema - relock always
	 */
	public static final int RELOCK_ALWAYS = 0;
	/**
	 * Auto-relock schema - relock only when no headset is connected
	 */
	public static final int RELOCK_NO_HEADSET = 1;
	/**
	 * Auto-relock schema - never relock
	 */
	public static final int RELOCK_NEVER = 2;

	/**
	 * Wake lock with proximity sensor (hidden value)
	 */
	private final int PROXIMITY_WAKE_LOCK = 32;
	/**
	 * Vibration patter for locking and unlocking
	 */
	private final long[] PATTERN_LOCK_UNLOCK = {0, 200};
	/**
	 * Vibration pattern for "just before locking" vibration
	 */
	private final long[] PATTERN_ABOUT_LOCK = {0, 100, 200, 100};
	/**
	 * "Just before locking" vibration time adjustment
	 */
	private final int REACTION_TIME = 800; //ms
	/**
	 * unlock() has been called most recently
	 */
	private final int STATUS_UNLOCKED = 0;
	/**
	 * lock() has been called most recently
	 */
	private final int STATUS_LOCKED = 1;
	/**
	 * Neither lock() nor unlock() has been called yet (initial state)
	 */
	private final int STATUS_INVALID = 2;
	/**
	 * Wakelock tag
	 */
	private final String WAKELOCK_TAG = "SmartLock2_PROXIMITY_WAKE_LOCK_TAG";

	/**
	 * Delayed tasks execution management object
	 */
	private Handler mHandler=null;
	/**
	 * Current status
	 */
	private int mStatus = STATUS_INVALID;
	/**
	 *  Device lock delay
	 */
	private int mLockDelay;
	/**
	 * Vibrator service
	 */
	private Vibrator mVibrator = null;
	/**
	 * Current vibration schema
	 */
	private int mVibrateSchema = NO_VIBRATE;
	/**
	 * Current Wakelock object
	 */
	private boolean mLockLandscape = false;
	private boolean mSupressVibration = false;
	private PowerManager mPowerManager = null;
	private WakeLock mWakeLock = null;
	private boolean lockWhenCallEnds = false;
	private SmartLock2Trace mTrace = null;
	private BroadcastReceiver mScreenReceiver = null; 

	// Workarounds for Bluetooh voice control
	private boolean btwoEnabled = true;
	private boolean emulateKeyevent = true;
	private boolean btwoBreakSystemLock = true;
	private int btwoSwitchDelay = -1;
	private KeyguardManager	btwoKeyguardManager = null;
	@SuppressWarnings("deprecation")
	private KeyguardManager.KeyguardLock btwoKeyguardLock = null;
	private WakeLock btwoWakeLock = null;
	private DevicePolicyManager btwoDevicePolicyManager = null;
	private BluetoothListener mBTListener = null;
	private int mRelockMode = RELOCK_ALWAYS;
	private int mRelockDelay;
	
	/**
	 * Public lock function (for use by PhoneStateReceiver)
	 * @param immediate relock immediately (with normal vibration) or delayed (with no vibration)
	 */
	public void lockCallback(boolean immediate) {
		if (immediate) {
			mSupressVibration = false;
			lock();
		} else {
			mSupressVibration = true;
			lockDelayed(mRelockDelay);
		}
		Log.d("### mSupressVibration=%b, immediate = %b", mSupressVibration, immediate);
	}

	/**
	 * Check if vibration flag is set
	 * @param flag vibration flag
	 * @return <b>true<b> if flag is set
	 */
	private boolean isVibrationFlagSet(int flag) {
		return ((mVibrateSchema & flag) == flag);
	}

	/**
	 * Calculate sum of elements in the array
	 * @param elems input array
	 * @return sum of all elements
	 */
	private long sum(long[] elems) {
		long sum = 0;
		for (long elem: elems)
			sum += elem;
		return sum;
	}

	/**
	 * Check if phone status is valid for locking the screen with this app
	 * @return <b>true</b> if device can be locked now
	 */
	private boolean verifyLockConditions()
	{
		boolean phoneOnCall = SmartLock2PhoneStateReceiver.phoneOnCall();
		if (phoneOnCall) {
			Log.d("Registering callback");
			lockWhenCallEnds = true;
			SmartLock2PhoneStateReceiver.registerCallback(this);
		}
		return ((getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT || mLockLandscape) 
				&& !phoneOnCall
				&& (mStatus != STATUS_LOCKED));
	}

	/**
	 * Check if phone status is valid for locking the screen with this app
	 * @return <b>true</b> if device can be locked now
	 */
	@SuppressWarnings("deprecation")
	private boolean verifyRelockConditions()
	{
		Context context = getApplicationContext();
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		Log.d("Checking relock conditions: mode=%d, delay=%d, BT headset=%b, wired headset=%b", mRelockMode, mRelockDelay, 
				mBTListener.isHeadsetConnected(),  am.isWiredHeadsetOn());
		switch (mRelockMode) {
		case RELOCK_ALWAYS:
			return true;
		case RELOCK_NEVER:
			return false;
		case RELOCK_NO_HEADSET:
			return !(mBTListener.isHeadsetConnected() || am.isWiredHeadsetOn()); 
		}
		mBTListener.close();
		return false;
	}

	/**
	 * Schedule the task for delayed execution
	 * @param task task to be scheduled
	 * @param delay task delay
	 */
	private void scheduleTask(Runnable task, int delay)
	{

		try {
			mHandler.removeCallbacks(task);
			mHandler.postDelayed(task, delay);
			Log.d("Scheduling background task to be run in %d ms", delay);
		} 
		catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Cancel all scheduled tasks
	 */
	private void cancelScheduledTasks()
	{
		Log.d("Cancelling scheduled task");
		mHandler.removeCallbacksAndMessages(null);
		if (lockWhenCallEnds) {
			Log.d("Cancelling callback");
			lockWhenCallEnds = false;
			SmartLock2PhoneStateReceiver.registerCallback(null);
		}
		mSupressVibration = false;
		Log.d("### mSupressVibration=%b", mSupressVibration);
	}

	private boolean verifyVibrateConditions()
	{
		Log.d("### mSupressVibration: %b", mSupressVibration);
		Log.d("### (((AudioManager)getSystemService(Context.AUDIO_SERVICE)).getRingerMode()=%d", ((AudioManager)getSystemService(Context.AUDIO_SERVICE)).getRingerMode() );
		if (mSupressVibration)
			return false;
		return (((AudioManager)getSystemService(Context.AUDIO_SERVICE)).getRingerMode() != AudioManager.RINGER_MODE_SILENT);
	}
	/**
	 * Vibrate 
	 * @param pattern vibration pattern
	 */
	private void vibrate(final long[] pattern) {
		Log.d("Vibrate invoked");
		if (mVibrator != null)
			if (verifyVibrateConditions())
				mVibrator.vibrate(pattern, -1);
			else
				Log.d("Vibration not invoked due to unsatisfied conditions");
		else
			Log.e("Vibration requested when vibration service handle not available:(. This should not happen");
	}

	/**
	 * Schedule vibration task
	 * @param delay task delay
	 * @param pattern vibration patter
	 */
	private void vibrateDelayed(int delay, final long[] pattern) {
		//check requested delay first (calculations may result in negative number - in such case we decide
		//not to vibrate at all)
		if (delay < 0)
			return;
		Log.d("Vibrating in %d ms", delay);
		scheduleTask(new Runnable() {
			public void run() {
				vibrate(pattern);
			}
		}, delay);
	}

	/**
	 * Lock device immediately
	 */
	@SuppressLint("Wakelock")
	@SuppressWarnings("deprecation")
	private void lock() {
		if (isVibrationFlagSet(VIBRATE_ON_LOCK_UNLOCK))
			vibrate(PATTERN_LOCK_UNLOCK);
		try {
			Thread.sleep(sum(PATTERN_LOCK_UNLOCK));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (emulateKeyevent)
		{
			lockWithSu(true);
		}
		else
		{
			if (mWakeLock != null  && !mWakeLock.isHeld())
				mWakeLock.acquire();
		}
		mStatus = STATUS_LOCKED;
		Log.w("Device locked");
		mTrace.write("Device locked");
		if (verifyRelockConditions())
			SmartLock2ScreenReceiver.registerCallback(this);
		// Workaround for Bluetooh voice control
		if (btwoEnabled) {
			if (btwoWakeLock != null  && btwoWakeLock.isHeld())
				btwoWakeLock.release();
			Log.d("Changing Proximity lock to DevicePolicyManager one");
			if (btwoKeyguardLock != null) 
				btwoKeyguardLock.reenableKeyguard();
			else
				btwoKeyguardLock = btwoKeyguardManager.newKeyguardLock("");
			btwoDevicePolicyManager.lockNow();
			Log.d("DevicePolicyManager lock acquired");
			try {
				Thread.sleep(btwoSwitchDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (mWakeLock != null  && mWakeLock.isHeld())
				mWakeLock.release();
			Log.d("Proximity lock relesed");
		}
		mSupressVibration = false;
	}

	/**
	 * Schedule device lock
	 * @param delay lock delay
	 */
	private void lockDelayed(int delay)
	{
		Log.d("Locking device in %d ms", delay);
		scheduleTask(new Runnable() {
			public void run() {
				lock();
			}
		}, delay);
	}

	/**
	 * Unlock device immediately
	 */
	@SuppressLint("Wakelock")
	@SuppressWarnings("deprecation")
	private void unlock() {
		Log.d("Vibrate schema is %d", mVibrateSchema);
		if (mStatus != STATUS_LOCKED)
			return;
		if (isVibrationFlagSet(VIBRATE_ON_LOCK_UNLOCK))
			vibrate(PATTERN_LOCK_UNLOCK);
		if (btwoEnabled) {
			// Workaround for Bluetooh voice control
			btwoWakeLock.acquire();
			Log.d("Disabling keyquard");
			btwoKeyguardLock.disableKeyguard();
			if (btwoBreakSystemLock) {
				Log.d("Exiting keyquard");
				btwoKeyguardManager.exitKeyguardSecurely(null);
			}
			//btwoKeyguardLock = null;
		} else {
			if (emulateKeyevent)
			{
				lockWithSu(false);
			}
			else
			{
				if (mWakeLock != null  && mWakeLock.isHeld())
					mWakeLock.release();
			}
		}
		SmartLock2ScreenReceiver.registerCallback(null);
		mStatus = STATUS_UNLOCKED;
		Log.w("Device unlocked");
		mTrace.write("Device unlocked");
	}

	private void lockWithSu(boolean lock)
	{
		Log.d("Executing keyevent");
	    Process p; 
	    // check if screen is on
	    boolean screenOn = mPowerManager.isScreenOn();
	    Log.d("lockWithSu: 'lock'=%b, 'screenOn'=%b, if condition=%b", lock, screenOn, (lock ^ screenOn));
	    if (lock ^ screenOn)
	    	// do nothing if requested screen state is same as actual
	    	return;
	    try {   
	       // Preform su to get root privledges  
	       p = Runtime.getRuntime().exec("su");   
	         
	       // Emulate pressing LOCK key   
	       DataOutputStream os = new DataOutputStream(p.getOutputStream());   
	       os.writeBytes("/system/bin/input keyevent 26\n");  
	         
	       // Close the terminal  
	       os.writeBytes("exit\n");   
	       os.flush();   
	       try {   
	          p.waitFor();   
	       }
	       catch (InterruptedException e)
	       {   
	       }   
	    }
	    catch (IOException e)
	    {   
	    }  
	}
	/**
	 * Handle proximity sensor events
	 * @param near <b>true</b> is sensor reported a NEAR event, <b>false</b> otherwise
	 */
	private void onSensorEvent(boolean near)
	{
		if (near) {
			Log.d("Screen covered");
			if (verifyLockConditions()) {  
				Log.i("Conditions matched, device will be locked in %d ms", mLockDelay);
				int delay = mLockDelay;
				if (isVibrationFlagSet(VIBRATE_ABOUT_LOCK)) {
					//from locking delay time, subtract pattern length and reaction time
					delay -= sum(PATTERN_ABOUT_LOCK);
					vibrateDelayed(delay - REACTION_TIME, PATTERN_ABOUT_LOCK);
				}
				lockDelayed(delay);
			}
		} 
		else 
		{
			//cancel any scheduled tasks
			cancelScheduledTasks();
			//unlock immediately
			unlock();
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	/**
	 * Service constructor
	 */
	@Override
	public void onCreate() {
		// scheduled tasks handler
		mHandler = new Handler();
		// register for receiving proximity sensor events
		// create proximity sensor wake lock
		mPowerManager = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PROXIMITY_WAKE_LOCK, WAKELOCK_TAG);
		mWakeLock.setReferenceCounted(false);
		Log.i("Service created");
	}

	/**
	 * Start the service
	 */
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Read all required data from preferences
		Context context = getApplicationContext();
		mBTListener = new BluetoothListener(context);
		mTrace = new SmartLock2Trace(this, getString(R.string.tracelog_filename));
		Log.i("Service started");
		if (!Restart) {
			mTrace.write("Service started");
			//Toast.makeText(this, "SmartLock2 service started", Toast.LENGTH_SHORT).show();
		} else 
			mTrace.write("Service restarted");
		Restart = true;
		loadPrefs();
		Log.d("mLockDelay=%d,mVibrateSchema=%d; BT Workaround: status=%b, delay=%d",mLockDelay,mVibrateSchema, btwoEnabled, btwoSwitchDelay);
		//NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		//mNotificationManager.cancel(1234);
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		mScreenReceiver = new SmartLock2ScreenReceiver();
		registerReceiver(mScreenReceiver, filter);
		return START_STICKY;
	}

	public void onLowMemory() {
		Log.i("Service low memory");
		mTrace.write("Service low memory");
		//Toast.makeText(this, "SmartLock2 service low memory", Toast.LENGTH_SHORT).show();
	}

	public void onDestroy() {
		mBTListener.close();
		if (mWakeLock != null  && mWakeLock.isHeld())
			mWakeLock.release();
		if (mScreenReceiver != null)
			unregisterReceiver(mScreenReceiver);
		String cause = "stopped"; 
		if (Restart)
			cause = "destroyed";
		Log.i("Service " + cause);
		mTrace.write("Service " + cause);
		//Toast.makeText(this, "SmartLock2 service " + cause, Toast.LENGTH_SHORT).show();
		if (Restart)
			start(this);
	}

	
	private boolean handleKey(String key, String value)
	{
		return (key == null || key.equals(value));
	}
	
	private void loadPrefs() {
		// load all prefs (service init)
		Log.d("Loading preferences");
		onPrefsChanged(null);
	}

	private void onPrefsChanged(String key) {
		Log.d("Preferences changed");
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (handleKey(key, "lock_screen_delay")) {
			try {
				mLockDelay = Integer.parseInt(prefs.getString("lock_screen_delay", getString(R.string.default_lock_screen_delay)));
				Log.d("Lock delay changed to %d ms", mLockDelay);
			} catch (Throwable e) {
				Log.e("Cannot set lock delay! Service communication failure? %s", e);
			}
			// Prefs changed case - one pref at a time, we can finish
			if (key != null) return;
		}
		if (handleKey(key, "lock_landscape")) {
			try {
				mLockLandscape = prefs.getBoolean("lock_landscape", Boolean.parseBoolean(getString(R.string.default_lock_landscape)));
			} catch (Throwable e) {
				Log.e("Cannot set lock landscape! Service communication failure? %s", e);
			}
			if (key != null) return;
		} 
		if (handleKey(key, "lock_unlock_vibrate") || handleKey(key, "about_lock_vibrate")) {
			try {
				mVibrateSchema = SmartLock2Service.NO_VIBRATE;
				if (prefs.getBoolean("lock_unlock_vibrate", Boolean.getBoolean(getString(R.string.default_lock_unlock_vibrate)))) {
					mVibrateSchema |= SmartLock2Service.VIBRATE_ON_LOCK_UNLOCK;
					if (prefs.getBoolean("about_lock_vibrate", Boolean.getBoolean(getString(R.string.default_about_lock_vibrate)))) {
						mVibrateSchema |= SmartLock2Service.VIBRATE_ABOUT_LOCK;
					}
				}
				Log.d("Vibrate schema changed: lock/unlock=%b, about=%b", isVibrationFlagSet(VIBRATE_ON_LOCK_UNLOCK), isVibrationFlagSet(VIBRATE_ABOUT_LOCK));
				if (mVibrateSchema != NO_VIBRATE) {
					if (mVibrator == null) {
						mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
						Log.d("Acquiring vibrator service");
					}
				} else {
					Log.d("Releasing vibrator service");
					mVibrator = null;
				}
			} catch (Throwable e) {
				Log.e("Cannot set vibrate schema! Service communication failure? %s", e);
			}
			if (key != null) return;
		}
		if (handleKey(key, "bt_workaround") || handleKey(key, "lock_switch_delay")  || handleKey(key, "break_system_lock")) {
			try {
				if (prefs.getBoolean("bt_workaround", false)) {
					btwoEnabled = true;
					btwoBreakSystemLock = prefs.getBoolean("break_system_lock", false);
					btwoSwitchDelay = Integer.parseInt(prefs.getString("lock_switch_delay", getString(R.string.default_lock_switch_delay)));
					btwoKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
					btwoWakeLock = mPowerManager.newWakeLock((PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), WAKELOCK_TAG);
					btwoDevicePolicyManager = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
				} else {
					btwoEnabled = false;
					btwoBreakSystemLock = false;
					btwoSwitchDelay = -1;
					btwoKeyguardManager = null; btwoKeyguardLock = null; btwoWakeLock = null; btwoDevicePolicyManager = null;
				}
			} catch (Throwable e) {
				Log.e("Cannot set lock delay! Service communication failure? %s", e);
			}
			if (key != null) return;
		}
		if (handleKey(key, "relock_mode") || handleKey(key, "relock_delay")) {
			try {
				mRelockMode = Integer.parseInt(prefs.getString("relock_mode", getString(R.string.default_relock_mode)));
				mRelockDelay = Integer.parseInt(prefs.getString("relock_delay", getString(R.string.default_relock_screen_delay)));
			} catch (Throwable e) {
				Log.e("Cannot set lock delay! Service communication failure? %s", e);
			}
			if (key != null) return;
		}
		if (handleKey(key, "emulate_keyevent"))
		{
			emulateKeyevent=prefs.getBoolean("emulate_keyevent", false);
			btwoEnabled = !emulateKeyevent;
			if (emulateKeyevent)
			{
				// get root privileges
			    try {
				    // Execute command in elevated shell to get root privileges  
			    	Process p = Runtime.getRuntime().exec("su");   
			       DataOutputStream os = new DataOutputStream(p.getOutputStream());   
			       os.writeBytes("/system/bin/input\n");  
			       // Close the terminal  
			       os.writeBytes("exit\n");   
			       os.flush();   
			       try {   
			          p.waitFor();   
			       } 
			       catch (InterruptedException e) 
			       {   
			       }   
			    } 
			    catch (IOException e) 
			    {   
			    }  
			}
			if (key != null) return;
		}
	}
	/**
	 * Helper function to start the service
	 * @param context current application context
	 */
	public static void start(Context context)
	{
		context.startService(new Intent(context, SmartLock2Service.class));
	}

	/**
	 * Helper function to start the service
	 * @param context current application context
	 * @param connection connection interface to newly created service
	 */
	public static void start(Context context, ServiceConnection connection)
	{
		start(context);
		connect(context, connection);
	}
	
	/**
	 * Helper function to stop the service
	 * @param context current application context
	 */
	public static void stop(Context context)
	{
		Restart = false;
		context.stopService(new Intent(context, SmartLock2Service.class));
	}
	
	/**
	 * Helper function to stop the service
	 * @param context current application context
	 * @param connection connection interface to the service
	 */
	public static void stop(Context context, ServiceConnection connection)
	{
		stop(context);
		disconnect(context, connection);
	}

	/**
	 * Helper function to connect the service
	 * @param context current application context
	 * @param connection connection interface to the service
	 */
	public static void connect(Context context, ServiceConnection connection)
	{
		context.bindService(new Intent(context,SmartLock2Service.class), connection, Context.BIND_AUTO_CREATE);
	}

	/**
	 * Helper function to disconnect the service
	 * @param context current application context
	 * @param connection connection interface to the service
	 */
	public static void disconnect(Context context, ServiceConnection connection)
	{
		context.unbindService(connection);
	}
}
