package com.greggor.SmartLock2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import android.content.Context;

/**
 * Keep some traces in separate, non-rotated file
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2Trace {
	
	private String mFilename = null;
	private Context mContext = null;
	/**
	 * Default constructor
	 * @param context application context, needed for I/O operations
	 * @param filename log file name
	 */
	public SmartLock2Trace(Context context, String filename) {
		mContext = context;
		mFilename = filename;
	}

	/**
	 * Write message to trace file
	 * @param message message to be written
	 */
	public void write(String message) {
		write(message, Context.MODE_APPEND);
	}
	
	/**
	 * Clear the log on user demand
	 */
	public void truncate() {
		write(null,Context.MODE_PRIVATE);
	}
	
	/**
	 * Write message to log file - low level
	 * @param message message to be written
	 * @param mode file creation mode (append during normal operation, create during truncate)
	 */
	private void write(String message, int mode) {
		try {
			FileOutputStream fos = mContext.openFileOutput(mFilename, mode);
			if (message != null) { 
				fos.write(String.format("[%1$td-%1$tm-%1$tY %1$tH:%1$tM:%tS] %s\n", (new Date()).getTime(), message).getBytes());
			} else 
				fos.write(0);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Read current log data
	 * @return log data as a single String object
	 */
	public String read() {
		StringBuffer fileContent = new StringBuffer();
		try {
			FileInputStream fis = mContext.openFileInput(mFilename);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = fis.read(buffer)) != -1) {
			    fileContent.append(new String(buffer, 0, length));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String s = new String(fileContent);
		if (s.length() < 5)
			return "<none>";
		else
			return fileContent.toString();
	}
}
