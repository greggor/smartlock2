/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.preference;

import com.greggor.SmartLock2.SmartLock2Trace;
import com.greggor.SmartLock2.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Clear events log dialog screen
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 * 
 */
public class SmartLock2ClearLogDialog
{

		public static void message (final Context context) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage("Clear entire log?")
			       .setCancelable(false)
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			   			SmartLock2Trace trace = new SmartLock2Trace(context, context.getString(R.string.tracelog_filename));
						trace.truncate();
						Toast.makeText(context, "Log cleared", Toast.LENGTH_SHORT).show();
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
