/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.preference;


import com.greggor.SmartLock2.R;
import com.greggor.SmartLock2.SmartLock2Trace;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Preferences dialog screen
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 * 
 */
public class SmartLock2EventsDialog extends DialogPreference {

	private Context mContext = null;

	/**
	 * Autogenerated constructor
	 * @param context application context
	 * @param attrs attributes
	 */
	public SmartLock2EventsDialog(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	/**
	 * Autogenerated constructor
	 * @param context application context
	 * @param attrs attributes
	 * @param defStyle style
	 */
	public SmartLock2EventsDialog(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}
	
	/**
	 * Create more fancy About dialog
	 */
	protected  View onCreateDialogView()
	{
		final TextView text = new TextView(mContext);
		// put some margin around the text
		text.setPadding(10, -5, 0, -5);
		SmartLock2Trace trace = new SmartLock2Trace(mContext, mContext.getString(R.string.tracelog_filename));
		text.setText(trace.read()); 
		final ScrollView scroller = new ScrollView(mContext);
		scroller.addView(text);
		return scroller;
	}
	
	public void onClick (DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			SmartLock2ClearLogDialog.message(mContext);
		}
	}
}
