/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.preference;

import com.greggor.SmartLock2.R;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

/**
 * Preference management using Fragments
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2SettingsFragment extends PreferenceFragment {
    @Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Load the preferences from an XML resource
    addPreferencesFromResource(R.xml.preferences);
    Preference button = (Preference)findPreference("clearLogButton");
    button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference arg0) { 
                        //code for what you want it to do   
                    	SmartLock2ClearLogDialog.message(arg0.getContext());
                        return true;
                    }
                });
}


}