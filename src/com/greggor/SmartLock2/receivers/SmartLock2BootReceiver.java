/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.receivers;

import com.greggor.SmartLock2.SmartLock2Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Allows application to be loaded after system boot is completed.
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class SmartLock2BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		//start on phone boot if enabled
		if (prefs.getBoolean("enabled",false)) 
		{
			SmartLock2ProximityReceiver.start(context);
			SmartLock2Service.start(context);
		}
	}

}
