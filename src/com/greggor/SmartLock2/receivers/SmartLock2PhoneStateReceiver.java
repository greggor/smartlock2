/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.receivers;

import com.greggor.SmartLock2.SmartLock2Service;
import com.greggor.log.Log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

/**
 *  Incoming call handler class
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2PhoneStateReceiver extends BroadcastReceiver {
	private static boolean phoneOnCall = false;
	private static SmartLock2Service mCallback = null;
	
	/**
	 * Phone call status
	 * @return <b>true</b> if in call, <b>false</b> otherwise
	 */
	public static synchronized boolean phoneOnCall() {
		return phoneOnCall;
	}
	
	/**
	 * Request locking the device as soon as phone gets to IDLE state
	 * @param callbackObj object that will lock the device
	 */
	public static synchronized void registerCallback(SmartLock2Service callbackObj) 
	{
		Log.d("Registered callback %s", callbackObj);
		mCallback = callbackObj;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// Check phone state
		String phone_state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		if ((phone_state.equals(TelephonyManager.EXTRA_STATE_RINGING) || phone_state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))) {
			Log.d("Phone on call");
			phoneOnCall = true;
		} else {
			Log.d("Phone not in the call");
			phoneOnCall = false;
			if (mCallback != null) {
				Log.d("Invoking lock callback");
				mCallback.lockCallback(true);
				Log.d("Clearing lock callback");
				mCallback = null;
			}
		}
			
	}
}
