/*
 * SmartLock2
 * Lock / unlock the device using proximity sensor.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.SmartLock2.receivers;

import com.greggor.log.Log;
import com.greggor.SmartLock2.R;
import com.greggor.SmartLock2.SL2SInterface;
import com.greggor.SmartLock2.SmartLock2Activity;
import com.greggor.SmartLock2.SmartLock2Service;
import com.greggor.SmartLock2.SmartLock2Trace;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Triggers lock request basing on proximity sensor events.
 * 
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2ProximityReceiver extends Service implements SensorEventListener {

	/**
	 * Service id
	 */
	private final int FOREGROUND_SERVICE_ID = 1234;
	/**
	 * Distance values lower than below are treated as near.
	 */
	private final float MIN_DISTANCE = 0.01f;
	/**
	 * Sensor manager
	 */
	private SensorManager mSensorManager=null;
	private SmartLock2Trace mTrace = null;
	/**
	 * Interface to service
	 */
	private SL2SInterface mServiceInterface;

	/**
	 * Service connection
	 */
    private ServiceConnection mConnection = new ServiceConnection()
    {
            public void onServiceConnected(ComponentName className, IBinder service) {
            	mServiceInterface = SL2SInterface.Stub.asInterface((IBinder)service);
            	Log.d("Service connected");
            }
     
            public void onServiceDisconnected(ComponentName className) {
            	mServiceInterface = null;
            	Log.d("Service disconnected");
            }

    };
	/**
	 * Checks sensor event type
	 * @param event sensor event
	 * @return <b>true</b> for NEAR event, <b>false</b> otherwise
	 */
	private Boolean isNear(SensorEvent event)
	{
		return (event.values[0] <= MIN_DISTANCE);
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	private void handleEvent(boolean near) {
		if (mServiceInterface == null)
			return;
		try {
			mServiceInterface.handleEvent(near);
		} catch (RemoteException e) {
			Log.d("Worker service offline, reconnecting");
			SmartLock2Service.connect(this, mConnection);
			try {
				mServiceInterface.handleEvent(near);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				Log.e("Could not reconnect worker service");
				e1.printStackTrace();
			}
		}
	}
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType()==Sensor.TYPE_PROXIMITY){
			Log.i("Received status %s", (isNear(event) ? "NEAR" : "FAR"));
			if (isNear(event))
				handleEvent(true);
			else 
				handleEvent(false);
		}
	}

	/**
	 * Service constructor
	 */
	@Override
	public void onCreate() {
		// register for receiving proximity sensor events
		mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
		Log.i("Service created");
	}

	/**
	 * Start the service
	 */
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Read all required data from preferences
		mTrace = new SmartLock2Trace(this, getString(R.string.tracelog_filename));
		SmartLock2Service.connect(this, mConnection);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		mTrace.write("Receiver started");
		Log.i("Receiver started");
		Toast.makeText(this, "SmartLock2 started", Toast.LENGTH_SHORT).show();
		Notification.Builder builder = new Notification.Builder(this);
		if (prefs.getBoolean("show_notification", false)) {
			builder
			.setContentTitle("SmartLock2")
			.setContentText("Running")
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, SmartLock2Activity.class), 0));
		}
		startForeground(FOREGROUND_SERVICE_ID, builder.getNotification());
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		return START_STICKY;
	}

	public void onLowMemory() {
		Log.i("Receiver low memory");
		mTrace.write("Receiver low memory");
		//Toast.makeText(this, "SmartLock2 low memory", Toast.LENGTH_SHORT).show();
	}

	public void onDestroy() {
		mSensorManager.unregisterListener(this);
		SmartLock2Service.disconnect(this, mConnection);
		Log.i("Receiver destroyed");
		mTrace.write("Receiver destroyed");
		Toast.makeText(this, "SmartLock2 destroyed", Toast.LENGTH_SHORT).show();
	}

	/**
	 * Helper function to start the service
	 * @param context current application context
	 * @param connection connection interface to newly created service
	 */
	public static void start(Context context)
	{
		context.startService(new Intent(context, SmartLock2ProximityReceiver.class));
	}

	/**
	 * Helper function to stop the service
	 * @param context current application context
	 * @param connection connection interface to the service
	 */
	public static void stop(Context context)
	{
		context.stopService(new Intent(context, SmartLock2ProximityReceiver.class));
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new IllegalArgumentException("Service cannot be bounded");
	}
}
