package com.greggor.SmartLock2.receivers;

import com.greggor.SmartLock2.SmartLock2Service;
import com.greggor.log.Log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Screen on/off state changes receiver
 * @author Grzegorz Ożański (greggor@gmail.com)
 *
 */
public class SmartLock2ScreenReceiver extends BroadcastReceiver {
	// thanks Jason
	
	/**
	 * Interface to service
	 */
	private static SmartLock2Service mCallback = null;
    
	/**
	 * Request locking the device if screen becomes active
	 * @param callbackObj object that will lock the device
	 */
	public static synchronized void registerCallback(SmartLock2Service callbackObj) 
	{
		Log.d("Registered callback %s", callbackObj);
		mCallback = callbackObj;
	}

	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			// do whatever you need to do here
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			// and do whatever you need to do here
			if (mCallback != null) 
			{
				Log.d("Invoking lock callback");
				mCallback.lockCallback(false);
				Log.d("Clearing lock callback");
				mCallback = null;
			}
				
		}
	}

}
